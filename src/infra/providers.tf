
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.55.0"
    }
  }
}

terraform {
  backend "s3"{
    region         = "eu-west-1"
    bucket         = "streamlit-languagedetector-stage-bucket"
    key            = "infra/terraform.tfstate"
    encrypt        = "true"
    dynamodb_table = "streamlit-languagedetector-stage-state-lock"
    profile        = "terraform-cli"
  }
}

provider "aws" {}


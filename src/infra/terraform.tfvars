##########################
# Administrative Variables
##########################
platform          = "streamlit-languagedetector"
application       = "Streamlit-LanguageDetector"
environment       = "stage"
app_code          = "SLD"

##################################
# Network config Variables
##################################
server-sg-id      = "sg-0207d076cc52b93d2"
server-subnet-id  = "subnet-0aeea38c6d044e9b0"

##################################
# Server config Variables
##################################
keypair           = "runner-keypair"
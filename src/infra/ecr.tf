
resource "aws_ecr_repository" "infra_repository" {
   name                 = "${var.platform}-${var.environment}-repository"
   image_tag_mutability = var.image_tag_mutability
   
   image_scanning_configuration {
      scan_on_push = var.scan_on_push
   }
}
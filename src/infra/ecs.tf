
resource "aws_ecs_cluster" "infra_cluster" {
  name = "${var.platform}-${var.environment}-cluster"

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
}



##########################
# Creator
##########################

variable "created_by_email" {
    type        = string
    description = "Resource created by"
    default     = "domsdev@reverb.com"
}

##########################
# Global config Variables
##########################

variable "region" {
    description = "Region"
    type        = string
    default     = "eu-west-1"
}

variable "availability_zone" {
    description = "server availability zone"
    type        = string
    default     = "eu-west-1a"
}

##########################
# Administrative Variables
##########################

variable "platform" {
    description = "Platform Name"
    type = string
    validation {
        condition = lower(var.platform) == var.platform
        error_message = "Platform Name must be lower case"
    }
}

variable "application" {
    description = "Application Name"
    type = string
}

variable "environment" {
    description = "Environment type"
    type        = string
}

variable "app_code" {
  description = "Application trigram code"
  type        = string
}

variable "purpose" {
    type        = string
    description = "Application's purpose"
    default     = "Web application"
}

variable "contact_email" {
    description = "Contact Email Address"
    type        = string
    default     = "domsdev@reverb.com"
}

variable "owner_email" {
    description = "Owner Email Address"
    type        = string
    default     = "domsdev@reverb.com"
}

variable "business_function" {
    description = "Area of the business this resource belongs to"
    type        = string
    default     = "Learning"
}

variable "cost_center" {
    description = "To indicate where cost should be allocated"
    type        = string
    default     = "Personal"
}

##################################
# Network config Variables
##################################

variable "server-sg-id" {
    description = "security-group ID"
    type        = string
}

variable "server-subnet-id" {
    description = "subnet ID"
    type        = string
}

##################################
# Server config Variables
##################################

variable "registration_token" {
    description = "environment variable from repository CI/CD-Variables to register the app-runner-server"
    type = string
}

variable "instance_type" {
    description = "instance_type for the gitlab-runner server"
    type        = string
    default     = "t2.micro"
}

variable "ami" {
    description = "AMI ID for the EC2 instance"
    type        = string
    default     = "ami-0b752bf1df193a6c4" # Linux 2 ami
 
    validation {
        condition     = length(var.ami) > 4 && substr(var.ami, 0, 4) == "ami-"
        error_message = "Please provide a valid value for variable AMI."
    }
}

variable "keypair" {
    description = "keypair name for ssh connection to the server"
    type        = string
    sensitive   = true
}

##################################
# Infra ECR config Variables
##################################

variable "image_tag_mutability" {
    description = "The tag mutability setting for the repository. Use 'IMMUTABLE' OR 'MUTABLE'"
    type        = string
    default     = "IMMUTABLE"
}

variable "scan_on_push" {
    description = "Indicates whether images are scanned after being pushed to the repository (true) or not scanned (false)"
    type        = bool
    default = "true"
}

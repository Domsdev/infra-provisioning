
output "app-runner-private_ip" {
  value = ["${aws_instance.app-runner-server.*.private_ip}"]
}

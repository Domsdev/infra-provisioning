
resource "aws_s3_bucket" "backend-bucket" {
   bucket                = "${var.platform}-${var.environment}-bucket"

   tags = {
      Platform         = "${var.platform}"
      Application      = "${var.application}"
      Purpose          = "${var.purpose}"
      Environment      = "${var.environment}"
      ContactEmail     = "${var.contact_email}"
      OwnerEmail       = "${var.owner_email}"
      BusinessFunction = "${var.business_function}"
      CostCenter       = "${var.cost_center}"
   }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "backend-encrypt" {
   bucket = aws_s3_bucket.backend-bucket.id

   rule {
      apply_server_side_encryption_by_default {
         sse_algorithm = "AES256"
      }
   }
}

resource "aws_s3_bucket_public_access_block" "backend-access" {
   bucket = aws_s3_bucket.backend-bucket.id
   block_public_acls       = true
   block_public_policy     = true
   ignore_public_acls      = true
   restrict_public_buckets = true
}

resource "aws_dynamodb_table" "backend-state-lock" {
   name = "${var.platform}-${var.environment}-state-lock"
   hash_key = "LockID"
   read_capacity = 20
   write_capacity = 20
 
   attribute {
      name = "LockID"
      type = "S"
   }
}

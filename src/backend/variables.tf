
##########################
# Creator
##########################

variable "created_by_email" {
    type        = string
    description = "Resource created by"
    default     = "domsdev@reverb.com"
}

##########################
# Global config Variables
##########################

variable "region" {
    description = "Region"
    type        = string
    default     = "eu-west-1"
}

variable "availability_zone" {
    description = "server availability zone"
    type        = string
    default     = "eu-west-1a"
}

##########################
# Administrative Variables
##########################

variable "platform" {
    description = "Platform Name"
    type = string
    validation {
        condition = lower(var.platform) == var.platform
        error_message = "Platform Name must be lower case"
    }
}

variable "application" {
    description = "Application Name"
    type = string
}

variable "environment" {
    description = "Environment type"
    type        = string
}

variable "app_code" {
  description = "Application trigram code"
  type        = string
}

variable "purpose" {
    type        = string
    description = "Application's purpose"
    default     = "Web application"
}

variable "contact_email" {
    description = "Contact Email Address"
    type        = string
    default     = "domsdev@reverb.com"
}

variable "owner_email" {
    description = "Owner Email Address"
    type        = string
    default     = "domsdev@reverb.com"
}

variable "business_function" {
    description = "Area of the business this resource belongs to"
    type        = string
    default     = "Learning"
}

variable "cost_center" {
    description = "To indicate where cost should be allocated"
    type        = string
    default     = "Personal"
}

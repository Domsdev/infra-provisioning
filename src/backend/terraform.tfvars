##########################
# Administrative Variables
##########################
platform          = "streamlit-languagedetector"
application       = "Streamlit-LanguageDetector"
environment       = "stage"
app_code          = "SLD"

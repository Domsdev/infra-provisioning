##########################
# Administrative Variables
##########################
platform          = "streamlit-languagedetector"
application       = "Streamlit-LanguageDetector"
environment       = "stage"
app_code          = "SLD"

##################################
# Network config Variables
##################################
vpc_cidr_block    = "10.34.0.0/16"
subnet_cidr_block = "10.34.1.0/24"

##################################
# Server config Variables
##################################
keypair           = "runner-keypair"
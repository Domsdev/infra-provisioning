
resource "aws_vpc" "server-vpc" {
    cidr_block = "10.34.0.0/16"
    
    tags = {
        Name     = "${var.application}-vpc"
        Platform = "${var.platform}"
    }
}

resource "aws_subnet" "server-subnet" {
    vpc_id            = aws_vpc.server-vpc.id
    cidr_block        = "10.34.1.0/24"
    availability_zone = "${var.availability_zone}"
    
    tags = {
        Name     = "${var.application}-subnet"
        Platform = "${var.platform}"
    }
}

resource "aws_internet_gateway" "server-igw" {
    vpc_id = aws_vpc.server-vpc.id
    
    tags = {
        Name     = "${var.application}-igw"
        Platform = "${var.platform}"
    }
}

resource "aws_route_table" "server-rtb" {
    vpc_id = aws_vpc.server-vpc.id
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.server-igw.id
    }
    
    tags = {
        Name     = "${var.application}-rtb"
        Platform = "${var.platform}"
    }
}

resource "aws_route_table_association" "server-rtb-association" {
    subnet_id      = aws_subnet.server-subnet.id
    route_table_id = aws_route_table.server-rtb.id
}

resource "aws_security_group" "server-sg" {
    name        = "${var.application}-sg"
    description = "Allow necessary traffic"
    vpc_id      = aws_vpc.server-vpc.id

    # ingress {
    #     description = "inbound SSH to VPC"
    #     from_port   = 22
    #     to_port     = 22
    #     protocol    = "tcp"
    #     cidr_blocks = ["0.0.0.0/0"]
    # }

    ingress {
        description = "inbound TCP:8501 to VPC"
        from_port   = 8501
        to_port     = 8501
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        description = "inbounb TLS to VPC"
        from_port   = 443
        to_port     = 443
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
  }

    egress {
        description = "outbound All from vpc"
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
        Name     = "${var.application}-sg"
        Platform = "${var.platform}"
    }
}
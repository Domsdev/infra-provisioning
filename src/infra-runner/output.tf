
output "server-sg-id" {
  description = "security-group ID"
  value       = aws_security_group.server-sg.id
}

output "server-subnet-id" {
  description = "subnet ID"
  value       = aws_subnet.server-subnet.id
}

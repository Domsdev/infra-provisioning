
resource "aws_instance" "infra-runner-server" {
    ami                         = var.ami
    instance_type               = var.instance_type
    key_name                    = var.keypair
    associate_public_ip_address = true
    subnet_id                   = aws_subnet.server-subnet.id
    vpc_security_group_ids      = [aws_security_group.server-sg.id]
    
    tags = {
        Name             = "${var.application}-infra-runner-server"
        Platform         = "${var.platform}"
        Application      = "${var.application}"
        Purpose          = "${var.purpose}"
        Environment      = "${var.environment}"
        ContactEmail     = "${var.contact_email}"
        OwnerEmail       = "${var.owner_email}"
        BusinessFunction = "${var.business_function}"
        CostCenter       = "${var.cost_center}"
    }
    
    user_data = <<EOF
#!/bin/bash
yum update -y

# Docker install
amazon-linux-extras install docker -y
usermod -a -G docker ec2-user
systemctl enable docker
systemctl start docker

# Create Gitlab-runner volume
docker volume create gitlab-runner-config

# Run Gitlab-runner container
docker run -d --name gitlab-runner --restart always \
-v /var/run/docker.sock:/var/run/docker.sock \
-v gitlab-runner-config:/etc/gitlab-runner \
gitlab/gitlab-runner

# Gitlab-runner registration
docker run --rm -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner register \
--non-interactive \
--url "https://gitlab.com/" \
--registration-token "${data.local_sensitive_file.registration_token.content}" \
--executor "docker" \
--docker-image docker:20.10.16 \
--description "${var.platform}-infra-runner" \
--maintenance-note "Domsdev" \
--tag-list "${var.environment}" \
--run-untagged="true" \
--locked="false" \
--access-level="not_protected"

# Update Gitlab-runner config file
CONFIG_FILE=/var/lib/docker/volumes/gitlab-runner-config/_data/config.toml
sed -i '/\[runners.docker\]/,$d' $${CONFIG_FILE}

echo '
  [runners.docker]
    pull_policy = "if-not-present"
    tls_verify = false
    image = "docker:20.10.16"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
    shm_size = 0' > update_config.toml

cat update_config.toml >> $${CONFIG_FILE}
docker restart gitlab-runner
EOF
}
